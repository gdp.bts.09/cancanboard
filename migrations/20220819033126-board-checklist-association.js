'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    queryInterface.addConstraint('Checklists', {
      fields: ['board_id'],
      type: 'foreign key',
      name: 'fki_board_id',
      references: {
        table : 'Boards',
        field : 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await  queryInterface.removeConstraint('Checklists', 'fki_board_id');
  }
};