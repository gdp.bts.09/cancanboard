'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Member extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Member.belongsTo(models.User, {foreignKey: 'user_id' , constraints: false});
      // models.User.hasMany(Member, {foreignKey: 'user_id'});
      Member.belongsTo(models.Board, { foreignKey: "owner_id" , constraints : false , scope: {
        owner_type : 'Board'
      }});
      Member.belongsTo(models.Checklist, { foreignKey: "owner_id" , constraints : false , scope: {
        owner_type : 'Checklist'
      }});
    }
  }
  Member.init({
    user_id: DataTypes.INTEGER,
    owner_type: DataTypes.STRING,
    owner_id: DataTypes.INTEGER,
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Member',
  });
  return Member;
};