var express = require('express');
var router = express.Router();
const checklistController = require('../controller/checklist.controller');
const authMiddleware = require('../middleware/auth');

router.post('/', authMiddleware.auth, checklistController.addChecklist);
router.get('/:id' , authMiddleware.auth , checklistController.readChecklistById);
router.put('/:id' , authMiddleware.auth , checklistController.updateChecklist);
router.post('/:id/members' , authMiddleware.auth , checklistController.addChecklistMember);
router.delete('/:id/members/:idMembers' , authMiddleware.auth , checklistController.deleteChecklist);

module.exports = router; 