'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    queryInterface.addConstraint('Members', {
      fields: ['user_id'],
      type: 'foreign key',
      name: 'fki_user_id',
      references: {
        table : 'Users',
        field : 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await  queryInterface.removeConstraint('Members', 'fki_user_id');
  }
};
