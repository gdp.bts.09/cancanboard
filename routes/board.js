var express = require('express');
var router = express.Router();
const boardController = require('../controller/board.controller');
const checklistController = require('../controller/checklist.controller');
const authMiddleware = require('../middleware/auth');

//--board routes
router.post('/' , authMiddleware.auth ,boardController.addBoard);
router.get('/' , authMiddleware.auth ,boardController.getBoard);
router.get('/:id' , authMiddleware.auth ,boardController.getBoardById);
router.put('/:id' , authMiddleware.auth ,boardController.updateBoard);
router.post('/:id/members' , authMiddleware.auth ,boardController.addBoardMember);
router.put('/:id/members/:idMembers', authMiddleware.auth ,boardController.updateBoardMember);
router.delete('/:id/members/:idMembers', authMiddleware.auth ,boardController.deleteBoardMember);


//--checklist routes
router.get('/:id/checklists' , authMiddleware.auth , checklistController.readChecklist);


module.exports = router;
