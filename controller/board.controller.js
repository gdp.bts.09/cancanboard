const { Board, Member, User } = require('../models');
const t = require("../helper/transaction");


//-- CREATE BOARD 
async function addBoard(req, res, next) {
    // const tokenBearer = req.header('Authorization'); // pake Authorization bearer token
    // const token = tokenBearer.replace(/^Bearer\s/, ''); // remove si type bearer dan spasinya

    // const decoder = jwt.decode(token);
    const dataBoard = {
        name: req.body.name,
        isArchive: true,
        createdAt: new Date(),
        updatedAt: new Date()
    }
    const transaction = await t.create();
    try {
        const resultBoard = await Board.create(dataBoard);

        const dataMember = {
            user_id: req.user.id,
            owner_type: "Board",
            owner_id: resultBoard.id,
            role: "owner",
            createdAt: new Date(),
            updatedAt: new Date()
        }
        const result = await Member.create(dataMember, t);
        if (!dataMember) {
            t.rollback(dataMember);
            res.status(400).json({
                statusCode: '400',
                message: "cannot add board"
            })
        } else { // commit data member
            t.commit(dataMember);
            res.status(200).json({
                statusCode: '200',
                message: dataMember
            });
        }
    } catch (error) {
        res.status(400).json({
            statusCode: '400',
            message: "cannot add board"
        })
    }
}



//-- GET ALL BOARD THAT USER'S CREATED OR ASSIGNED
function getBoard(req, res, next) {

    Member.findAll({ where: { user_id: req.user.id }, owner_type: 'Board' }).then(resMember => {
        // const owner_id = resMember[0].owner_id;
        // console.log(resMember)
        Board.findAll({ include: { model: Member, attributes: [], where: { user_id: req.user.id } } }).then(boards => {
            // console.log(boards)

            res.status(200).json({ statusCode: "200", message: "Ok", boards });
        }).catch(err => {
            res.status('400').json({ message: "cannot find board" });
        });
    }).catch(err => {
        res.status('400').json({ message: "cannot find board" });
    });
}

//-- GET ALL BOARD THAT USER'S CREATED OR ASSIGNED BY ID
function getBoardById(req, res, next) {
    const idTarget = req.params.id;

    Board.findOne({ where: { id: idTarget }, include: { model: Member, where: { user_id: req.user.id } } }).then(boards => {
        console.log(boards)
        const data = {
            user_id: req.user.id,
            name: boards.name
        }
        res.status(200).json({ statusCode: "200", message: "Ok", data })
    }).catch(err => {
        res.status('400').json({ message: "cannot find board" });
        // res.send(err.message);
    });
}

//--UPDATE BOARD WITH ROLE ADMIN OR OWNER THAT CAN UPDATE, CURRENT USER ONLY CAN UPDATE THE BOARD THAT ASSIGNED TO THEM
async function updateBoard(req, res, next) {
    // const tokenBearer = req.header('Authorization'); // pake Authorization bearer token
    // const token = tokenBearer.replace(/^Bearer\s/, ''); // remove si type bearer dan spasinya

    // const decoder = jwt.decode(token);

    const idTarget = req.params.id;

    try {
        const getRole = await Member.findOne({ where: { user_id: req.user.id } })
        // console.log(getRole)

        if (getRole.role == 'owner') {
            const data = {
                name: req.body.name
            }
            Board.update(data, { where: { id: idTarget }, include: { model: Member, where: { user_id: req.user.id } } }).then(boards => {
                Board.findOne({ where: { id: idTarget }, include: { model: Member, where: { user_id: req.user.id } } }).then(findBoard => {
                    const result = {
                        id: findBoard.id,
                        name: findBoard.name
                    }
                    // console.log(boards) 
                    res.status(200).json({ statusCode: "200", message: "Ok", result })
                }).catch(err => {
                    res.status(400).json({
                        statusCode: '400',
                        message: "Failed to update board, role not owner/admin"
                    })
                });
            }).catch(err => {
                res.status(400).json({
                    statusCode: '400',
                    message: "Failed to update board, role not owner/admin"
                });
            });
        } else {
            res.status(400).json({
                statusCode: '400',
                message: 'Failed to update board, role not owner/admin'
            })
        }
    } catch (error) {
        res.status(400).json({
            statusCode: '400',
            message: 'Failed to update board, role not owner/admin'
        })
    }


}


//--CREATE MEMBER BOARD BY ID
async function addBoardMember(req, res, next) {

    //data yang diinput ke member
    const input = {
        email: req.body.email,
        owner_type: req.body.owner_type,
        owner_id: req.body.owner_id,
        role: req.body.role
    }
    const transaction = await t.create();
    try {
        const getRole = await Member.findOne({ where: { user_id: req.user.id } }, t)
        if (getRole.role == 'owner') {
            const resBoards = await Board.findOne({ where: { id: input.owner_id } }, t);
            // console.log(resBoards)
            const resUsers = await User.findOne({ where: { email: input.email } }, t);
            const data = {
                user_id: resUsers.id,
                email: resUsers.email,
                owner_type: input.owner_type,
                owner_id: input.owner_id,
                role: input.role,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            const resMembers = await Member.create(data, t, { include: { model: Member, where: { role: 'owner', owner_id: input.owner_id } } });
            const result = {
                id: resBoards.id,
                user_id: resUsers.id,
                email: resUsers.email,
                owner_type: input.owner_type,
                owner_id: input.owner_id,
                role: input.role
            }
            if (!data) {
                t.rollback(data, t);
                res.status(400).json({})
            } else { // commit data member
                t.commit(data, t);
                res.status(200).json({
                    statusCode: '200',
                    message: 'Ok',
                    result
                });
            }
        } else {
            t.rollback(data, t);
            res.status(400).json({
                statusCode: '400',
                message: 'Failed to add member role not owner/admin'
            })
        }

    } catch (error) {
        res.status(400).json({
            statusCode: '400',
            message: 'Failed to add member role not owner/admin'
        })
    }
}

//--UPDATE MEMBER YANG SUDAH DIASSIGN PADA BOARD DENGAN MEMBER ID
//-- jadi owner bisa nunjuk member buat gantiin role owner
async function updateBoardMember(req, res, next) {
    //udah bisa assign dari member ke owner tapi owner sebelumnya masih ada

    const role = req.body; //rolenya owner
    const idBoard = req.params.id;
    const idMembers = req.params.idMembers;

    const getRole = await Member.findOne({ where: { user_id: req.user.id , owner_id : idBoard } })

    if (getRole.role == 'owner') {
        // const resMember = await Member.findOne({ where: { user_id: idMembers } })

        const updateMember = await Member.update(role, { where: { id: idMembers , owner_id : idBoard} });
        // const updateOwner = await Member.update({ role: "member" }, { where: { id: req.user.id , owner_id : idBoard} });
        // console.log(updateOwner)
        const getUpdateMember = await Member.findOne({ where: { id: idMembers , owner_id : idBoard} });

        const data = {
            id: getUpdateMember.id,
            user_id: getUpdateMember.user_id,
            owner_type: getUpdateMember.owner_type,
            owner_id: getUpdateMember.owner_id,
            role: getUpdateMember.role
        }       
        
        res.status(200).json({
            statusCode: "200",
            message: "Ok",
            data
        })
    } else {
        res.status(400).json({
            statusCode: '400',
            message: 'Failed to add member role not owner/admin or member'
        })
    }
}

//--DELETE MEMBER YANG SUDAH DIASSIGN PADA BOARD DENGAN MEMBER ID
async function deleteBoardMember(req, res, next) {
    const idBoard = req.params.id;
    const idMembers = req.params.idMembers;

    const getRole = await Member.findOne({ where: { owner_id: idBoard } }, { include: { model: Board }, where: { id: idBoard } })

    console.log(idMembers)

    if (getRole.role == 'owner') {
        Member.destroy({ where: { id: idMembers, role: 'member' } }, { include: { model: Board }, where: { id: idBoard } }).then(result => {

            res.status(200).json({ statusCode: "200", message: "Ok" })
        }).catch(err => {
            res.status(400).send(err.message);
        });
    } else {
        res.status(400).json({
            statusCode: '400',
            message: 'Failed to remove member role not owner/admin or member'
        })
    }
}


module.exports = {
    addBoard,
    getBoard,
    getBoardById,
    updateBoard,
    addBoardMember,
    updateBoardMember,
    deleteBoardMember
}