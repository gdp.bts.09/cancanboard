'use strict';
const {
  Model
} = require('sequelize');
// const board = require('./board');
module.exports = (sequelize, DataTypes) => {
  class Checklist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //belum migrate database checklist karena associatenya belum(sudah)
      Checklist.belongsTo(models.Board , {foreignKey: "board_id"});
      Checklist.hasMany(models.Member , {foreignKey:"owner_id" });
      // models.Board.hasMany(models.Checklist);
    }
  }
  Checklist.init({
    board_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    due_date: DataTypes.DATE,
    labels: DataTypes.STRING,
    parent_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Checklist',
  });
  return Checklist;
};