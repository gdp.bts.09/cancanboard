const { Checklist, Board, Member, User } = require('../models');
const auth = require('../middleware/auth');
const t = require("../helper/transaction");
const { NONE } = require('sequelize');

//--FUNGSI UNTUK MENAMBAHKAN CHECKLIST
async function addChecklist(req, res, next) {
    //data buat input checklist
    const input = {
        name: req.body.name,
        board_id: req.body.board_id,
        board_name: req.body.board_name,
        description: req.body.description,
        due_date: req.body.due_date,
        labels: req.body.labels,
        parent_id: req.body.parent_id
    }
    const transaction = await t.create(); //transaksi dimulai 
    try {
        const dataBoard = await Board.findOne({ include: { model: Member, where: { user_id: req.user.id } } });
        // console.log(dataBoard)
        if (dataBoard.Members[0].role == 'member' || dataBoard.Members[0].role == 'owner') {
            const resultChecklist = await Checklist.create(input, t, { include: { model: Member, where: { role: 'owner', owner_id: input.owner_id } } }); //menampung hasil dari create checklist agar bisa diakses

            const dataMember = { //list data member yang akan dimasukkan
                user_id: req.user.id, //user mana yang melakukan aksi 
                owner_type: "Checklist",
                owner_id: resultChecklist.id, //diambil id dari checklist atau board
                role: "owner",
                createdAt: new Date(),
                updatedAt: new Date()
            }
            const result = await Member.create(dataMember, t, { include: { model: Member, where: { role: 'owner', owner_id: input.owner_id } } }); //menampunng hasil dari create agar bisa diakses
            if (!result) { //jika result tidak ada akan menjalankan transaksi rollback dan mengembalikan status error
                t.rollback(dataMember);
                res.status(400).json({})
            } else { // commit data member
                t.commit(dataMember);
                res.status(200).json({
                    statusCode: '200',
                    data: dataMember
                });
            }
        } else {
            res.status(400).json({ msg: "error else" })
        }
    } catch (error) {
        res.status(400).json({ message: "not assigned to board" })
    }
}

//--FUNGSI UNTUK MENDAPATKAN CHECKLIST YANG ADA PADA BOARD
function readChecklist(req, res, next) {

    const idTarget = req.params.id; //mengambil id pada end point /boads/:id/checklists

    Member.findAll({ where: { user_id: req.user.id }, owner_type: 'Board' }).then(resMember => {
        // const owner_id = resMember[0].owner_id;
        // console.log(resMember[0].owner_id)
        Checklist.findAll({where: { board_id: resMember[0].owner_id }
            // , include: { model: Member, include: { model: User, attributes: name }, attributes: []}
         }).then(checklists => {
            res.status(200).json({ statusCode: "200", message: "Ok", checklists });
        }).catch(err => {
            res.status('400').json({ message: "cannot find checklist" });
        });
    }).catch(err => {
        res.status('400').json({ message: "cannot find checklist" });
    });
}


//--FUNGSI UNTUK MENDAPATKAN CHECKLIST YANG ADA PADA BOARD BERDASARKAN CHECKLIST ID
function readChecklistById(req, res, next) {
    const idTarget = req.params.id; //untuk mendapatkan request id dari end point

    Member.findAll({ where: { user_id: req.user.id }, owner_type: 'Board' }).then(resMember => {
        // const owner_id = resMember[0].owner_id;
        // console.log(resMember[0].owner_id)
        Checklist.findOne({where: { board_id: resMember[0].owner_id }
            // , include: { model: Member, include: { model: User, attributes: name }, attributes: []}
         }).then(checklists => {
            res.status(200).json({ statusCode: "200", message: "Ok", checklists });
        }).catch(err => {
            res.status('400').json({ message: "cannot find checklist" });
        });
    }).catch(err => {
        res.status('400').json({ message: "cannot find checklist" });
    });

}


//--FUNGSI UNTUK UPDATE CHECKLIST 
async function updateChecklist(req, res, next) {

    const idTarget = req.params.id; //id yang ada pada request

    try {
        const getRole = await Member.findOne({ where: { user_id: req.user.id } })
        console.log(getRole.role)
        if (getRole.role == 'owner' || getRole.role == 'member' ) {
            const input = {
                name: req.body.name,               //
                description: req.body.description, //
                due_date: req.body.due_date,       // inputan untuk update data
                labels: req.body.labels,           //
                parent_id: req.body.parent_id      //
            }

            Checklist.update(input, { where: { id: idTarget }, include: { model: Member, where: { user_id: req.user.id } } }).then(result => {

                Checklist.findOne({ where: { id: idTarget }, include: { model: Member, where: { user_id: req.user.id } } }).then(checklists => {
                    const data = {
                        id: checklists.id,
                        name: checklists.name,
                        description: checklists.description,
                        due_date: checklists.due_date,
                        labels: checklists.labels,
                        parent_id: checklists.parent_id
                    }
                    res.status(200).json({ statusCode: "200", message: "Ok", data })
                }).catch(err => {
                    res.status(400).json({
                        statusCode: '400',
                        message: 'Failed to update checklist'
                    })
                });
            }).catch(err => {
                res.status(400).json({
                    statusCode: '400',
                    message: 'Failed to update checklist'
                })
            });
        } else {
            res.status(400).json({
                statusCode: '400',
                message: 'Failed to update checklist, role not owner/admin '
            })
        }
    } catch (error) {

    }


}

//--FUNGSI UNTUK MENAMBAH MEMBER PADA CHECKLIST
async function addChecklistMember(req, res, next) {

    const input = {
        email: req.body.email,
        owner_id: req.body.owner_id,
        owner_type: req.body.owner_type,
        role: req.body.role
    }

    try {
        const getRole = await Member.findOne({ where: { user_id: req.user.id } })
        if (getRole.role == 'owner') {
            const resChecklist = await Checklist.findOne({ where: { id: input.owner_id } });
            const resUsers = await User.findOne({ where: { email: input.email } })
            const data = {
                user_id: resUsers.id,
                email: resUsers.email,
                owner_type: input.owner_type,
                owner_id: input.owner_id,
                role: input.role,
                createdAt: new Date(),
                updatedAt: new Date()
            }
            await Member.create(data).then(checklists => {
                const result = {
                    id: resChecklist.id,
                    user_id: resUsers.id,
                    email: resUsers.email,
                    owner_type: input.owner_type,
                    owner_id: input.owner_id,
                    role: input.role
                }
                res.status(200).json({ statusCode: "200", message: "Ok", result })
            }).catch(err => {
                res.status(400).json({ msg: "error add member" });
                // res.status(400).send(err.message);
            });
        } else {
            res.status(400).json({ msg: "error, your role is not owner" });
        }
    } catch (error) {
        res.status(400).json({ msg: "error adding member" });
    }


}

//--FUNGSI UNTUK DELETE CHECKLIST
async function deleteChecklist(req, res, next) {
    const idChecklist = req.params.id
    const idMembers = req.params.idMembers;

    const getRole = await Member.findOne({ where: { owner_id: idChecklist } } , {include : {model : Checklist}, where : {id
        : idChecklist}})

    if (getRole.role == 'owner') {
        Member.destroy({ where: { user_id: idMembers  , role : 'member'}}, {include : {model : Checklist} , where : {id : idChecklist}}).then(result => {
            // Board.destroy({ where: { id: idTarget } }).then(boards => {
                res.status(200).json({ statusCode: "200", message: "Ok" })
            // }).catch(err => {
            //     res.status(400).send("error");
            // });
        }).catch(err => {
            res.status(400).send("error");
        });
    } else {
        res.status(400).json({
            statusCode: '400',
            message: 'Failed to remove member role not owner/admin or member'
        })
    }

    // Member.destroy({ where: { owner_id: idMembers, user_id: req.user.id, owner_type: 'Checklist' } }).then(result => {
    //     // Checklist.destroy({ where: { id: idTarget } }).then(res => {
    //     res.status(200).json({
    //         statusCode: "200",
    //         message: "Ok"
    //     })
    //     // }).catch(err => {
    //     //     res.send(err.message);
    //     // })
    // }).catch(err => {
    //     res.status(400).send(err.message);
    // });

}

module.exports = {
    addChecklist,
    readChecklist,
    readChecklistById,
    updateChecklist,
    addChecklistMember,
    deleteChecklist
}