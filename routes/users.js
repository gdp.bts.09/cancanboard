var express = require('express');
var router = express.Router();
const userController = require('../controller/user.controller');
const authMiddleware = require('../middleware/auth');

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

//get user profile
router.get('/profile' , authMiddleware.auth , userController.readById)

module.exports = router;
