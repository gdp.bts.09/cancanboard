'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    queryInterface.addIndex('Members', {
      fields: ['owner_id'],
      type: 'foreign key',
      name: 'fki_owner_id_checklist',
      references: {
        table : 'Checklists',
        field : 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await  queryInterface.removeIndex('Members', 'fki_owner_id');
  }
};
