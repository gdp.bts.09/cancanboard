'use strict';
const {
  Model
} = require('sequelize');
// const checklist = require('./checklist');
module.exports = (sequelize, DataTypes) => {
  class Board extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Board.hasMany(models.Checklist , {foreignKey: "board_id"});
      Board.hasMany(models.Member , {foreignKey:"owner_id" });
    }
  }
  Board.init({
    name: DataTypes.STRING,
    isArchive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Board',
  });
  return Board;
};