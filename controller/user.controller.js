const { User } = require('../models');

const auth = require('../middleware/auth');

const jwt = require('jsonwebtoken');

const Validator = require("fastest-validator");

const bcrypt = require("bcryptjs")

const v = new Validator({
    useNewCustomCheckerFunction: true, // using new version
    messages: {
        // Register our new error message text
        atLeastOneLetter: "The pass value must contain at least one letter from a-z and A-Z ranges!",
        atLeastOneDigit: "The pass value must contain at least one digit from 0 to 9!"
    }
});

const user = require('../models/user');

require('dotenv').config();
const JWT_SECRET = process.env.JWT_SECRET

//-- CREATE USER (register)
function register(req, res, next) {


    const data = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        createdAt: new Date(),
        updatedAt: new Date()
    }

    const schema = {
        name: { type: "string", min: 5, max: 50, optional: false },
        email: { type: "email", optional: false },
        password: { type: "string", custom: (v, errors) => {
            if (!/[0-9]/.test(v)) errors.push({ type: "atLeastOneDigit" });
            if (!/[a-zA-Z]/.test(v)) errors.push({ type: "atLeastOneLetter" });
            return v;
        } , min: 5, max: 255, optional: false},
    }

    //-- VALIDASI EMAIL
    User.findOne({ where: { email: req.body.email } }).then(user => {
        if (user) {
            // -- Email sudah digunakan
            res.status(400).json({
                message: 'Please check your email again '
            });
        } else {
            // -- Email belum digunakan

            // -- VALIDASI DATA
            const validationResult = v.validate(data, schema);

            if (validationResult !== true) {
                // -- Data tidak valid
                res.status(400).json({
                    message: 'Validation Failed',
                    data: validationResult
                });
            } else {
                // -- Create user jika email belum digunakan
                // -- Data valid dan bisa disimpan kedalam database
                User.create(data).then(result => {
                    res.status(201).json({
                        statusCode: '201',
                        message: 'Ok'
                    });
                }).catch(err => {
                    res.status(400).json({
                        message: 'Register Failed',
                        data: err
                    });
                });
            }
        }
    }).catch(err => {
        res.status(400).json({
            message: 'Something wrong',
            data: err
        });
    });



}

//-- READ USER 
function read(req, res, next) {
    User.findAll({
        where: { isDeleted: false }
    }).then(users => {
        res.status(200).json({ statusCode: "200", message: "Ok", users });
    }).catch(err => {
        res.send(err);
    });
}

//-- READ USER BY ID
function readById(req, res, next) {

    const userid = req.user.id;
    const name = req.user.name;
    const email = req.user.email;
    const sasa = req.headers.authorization;
    const token = sasa.split(' ')[1];

    User.findByPk(userid).then(users => {
        const data = {
            id: users.id,
            name: users.name,
            email: users.email,
            token: token
        }
        // console.log(users.length);
        if (users.length != 0) {
            res.status(200).json({ statusCode: "200", message: "Ok", data });
        } else {
            res.status(404)
            console.log("Empty");
        }
    }).catch(err => {
        res.send(err.message).json({});
    });
}

//-- UPDATE USER
function update(req, res, next) {
    const data = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        fullname: req.body.fullname,
        picture: req.body.picture,
        bio: req.body.bio,
        updatedAt: new Date(),
        updatedBy: req.user.id,
        isDeleted: false
    }

    const schema = {
        username: { type: "string", min: 5, max: 50, optional: false },
        email: { type: "email", optional: false },
        password: { type: "string", min: 5, max: 50, optional: false },
    }


    // -- VALIDASI DATA
    const validationResult = v.validate(data, schema);

    if (validationResult !== true) {
        // -- Data tidak valid
        res.status(400).json({
            message: 'Validation Failed',
            data: validationResult
        });
    } else {
        // -- Create user jika email belum digunakan
        // -- Data valid dan bisa disimpan kedalam database
        User.update(data, { where: { id: req.params.id } }).then(result => {
            res.status(200).json({
                message: 'Success update data',
                data: result
            });
        }).catch(err => {
            res.status(400).json({
                message: 'Updated Failed',
                data: err
            });
        });
    }


}

//-- DELETE USER
function deleteUser(req, res, next) {
    // -- DELETE RECORD
    // User.destroy({where : {id : req.params.id}}).then(result => {
    //     res.status(200).json({
    //         message : 'Delete data sukses',
    //         data: result
    //     });
    // }).catch(err => {
    //     res.status(400).json({
    //         message : 'Delete Failed',
    //         data: err
    //     });
    // });

    // -- SOFT DELETE
    const data = {
        isDeleted: true,
        deletedAt: new Date(),
        deletedBy: req.user.id
    }

    User.update(data, { where: { id: deletedBy } }).then(result => {
        res.status(200).json({
            message: 'Success delete data',
            data: result
        });
    }).catch(err => {
        res.status(400).json({
            message: 'Delete Failed',
            data: err
        });
    });
}

//-- LOGIN USER (SIGNIN)
function login(req, res, next) {
    User.findOne({ where: { email: req.body.email } }).then(user => {
        if (user) {
            // console.log(
            //     user.email, //
            //     user.id,    // ini kan object tapi kenapa gabisa dipanggil
            //     user.name   //
            // )
            const id = user.id
            const email = user.email
            const name = user.name
            // if (user.isDeleted == false) {

            bcrypt.compare(req.body.password, user.password, function (err, result) {
                if (result) {
                    // Pembuatan TOKEN saat login sukses
                    const token = jwt.sign({
                        email,
                        id,
                        name
                    }, JWT_SECRET, function (err, token) {
                        res.status(200).json({
                            statusCode: "200",
                            message: 'OK',
                            data: {
                                id,
                                name,
                                email,
                                token
                            }
                        });
                    });

                } else {
                    res.status(401).json({
                        status: "FAILED",
                        message: "Wrong Password",
                        data: err
                    })
                }
            })
            // } else {
            //     res.status(401).json({
            //         message: 'User has been deleted',
            //         data: user
            //     });
            // }
        } else {
            res.status(401).json({
                message: 'Please check again your email or password',
                data: user
            });
        }
    }).catch(err => {
        res.status(400).json({
            message: 'Login Failed',
            data: err
        });
    });

}

module.exports = {
    register,
    read,
    readById,
    update,
    deleteUser,
    login
}
