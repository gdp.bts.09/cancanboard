var express = require('express');
var router = express.Router();
const userController = require('../controller/user.controller');

//register user
router.post('/register' , userController.register)

//login user
router.post('/login' , userController.login)

module.exports = router;