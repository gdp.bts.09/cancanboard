'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    queryInterface.addIndex('Members', {
      fields: ['owner_id'],
      type: 'foreign key',
      name: 'fki_owner_id',
      references: {
        table : 'Boards',
        field : 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await  queryInterface.removeIndex('Members', 'fki_owner_id');
  }
};
